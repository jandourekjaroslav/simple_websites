var speed=100;

var hist=[];
var histIndex=hist.length;
$(document).ready( function(){
	$("#inp").keydown(keyhandler);
	greet();	
	    

});
async function greet(){

	element=$("#out");
	if(typeof(element) != 'undefined' && element != null){
		await new Promise(r => setTimeout(r, 500));
		var grt=["g","r","e","e","t","</br>"]
		for (var i=0;i<grt.length;i++){
			await new Promise(r => setTimeout(r, 50));
			$("#out").append(grt[i]);

		}
		await new Promise(r => setTimeout(r, 200));
		$("#out").append("Salutations <span id=\"highlightSpan\">visitor</span> ! Welcome to my personal website.<br>To start off with you may wanna type <span id=\"highlightSpan\">man</span> to see a list off available commands.<br>For a more normal looking style website use the <span id=\"highlightSpan\">gui</span> command.<br>")
		await new Promise(r => setTimeout(r, 20));
		$("#in").css("visibility","visible");
		$("#inp").focus();
	}


}
async function keyhandler(e){
//	console.log(e.which);
	var param=[];
	if (e.which ==13){
		command=$("#inp").val();
		const pattern=/^[a-zA-Z0-9 .\/_]+$/
		//check if all alphanumeric
		if (!pattern.test(command)){
			$("#out").append("Only <span id=\"highlightSpan\">alphanumeric</span> characters are supported!<br>");
			return 
		}
		hist.push(command);
		histIndex=hist.length
		$("#out").append("<span id=\"highlightSpan\">visitor</span><span id=\"greenSpan\">@</span>jj.cz<span id=\"greenSpan\">$</span>: "+command+"<br>");
		$("#inp").val("");
		param=(command.match(/[\w\.-/]+/g));
		command=param[0];
		console.log(command)
		switch(command){
			case "man":
				$("#out").append("<span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">man</span><span id=\"greenSpan\">&#09;&#09;</span> Prints this help<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">greet</span><span id=\"greenSpan\">&#09;&#09;</span> Prints the welcome message<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">gui</span><span id=\"greenSpan\">&#09;&#09;</span> Redirects to a more usual website design<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">clear</span><span id=\"greenSpan\">&#09;&#09;</span> Clears the terminal<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">about</span><span id=\"greenSpan\">&#09;&#09;</span> Prints a short info about the creator of this webpage<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">cv</span><span id=\"greenSpan\">&#09;&#09;</span> Shows the creator's CV. Requires an argument of \"pdf\" or \"cli\" to either show the PDF version or to print to CLI<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">git</span><span id=\"greenSpan\">&#09;&#09;</span> Redirects to the creator's GIT repository<br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">ls</span><span id=\"greenSpan\">&#09;&#09;</span> Lists either directories in <span id=\"highlightSpan\">/</span> or files in a <span id=\"highlightSpan\">directory</span> parameter. <br><span id=\"greenSpan\">-</span> <span id=\"highlightSpan\">cat</span><span id=\"greenSpan\">&#09;&#09;</span> Prints out content of a text file. Requires <span id=\"highlightSpan\">directory&filename (e.g. dir/file)</span> as a parameter.<br>")
				break
			case "clear":
				$("#out").html("")
				break
			case "":
				break
			case "gui":
				$(location).attr('href', 'index.html')
				break
			case "about":
				$("#out").append("I'm a 26 year old SysAdmin/Devops guy and FOSS enthusiast, currently self-employed as a Infrastructure Deployment and Management Consultant. You can find all of my work and education history in my CV alongside all other information regarding my skillset and past projects. <br>If so inclined check out my git repositories.<br>")
				break
			case "cv":
				cvHandler(param);
				//$("#out").append("command <span id=\"highlightSpan\">cv</span> requires either <span id=\"highlightSpan\">pdf</span> or <span id=\"highlightSpan\">cli</span> as a parametr<br>")
				break
			case "greet":
				$("#out").append("Salutations <span id=\"highlightSpan\">visitor</span> ! Welcome to my personal website.<br>To start off with you may wanna type <span id=\"highlightSpan\">man</span> to see a list off available commands.<br>For a more normal looking style website use the <span id=\"highlightSpan\">gui</span> command.<br>")
				break
			case "git":
				$(location).attr('href', 'https://gitlab.com/users/jandourekjaroslav/projects')
				break
			case "ls":
				$("#out").append(getFileLinks(param[1]));
				break
			case "cat":
				catHandler(param);

				//	$("#out").append("command <span id=\"highlightSpan\">cat</span> requires a f<span id=\"highlightSpan\">ilename</span> parametr. You can list files with the <span id=\"highlightSpan\">ls</span> command.<br>");
				break
			default:
				$("#out").append("<span id=\"highlightSpan\">"+command+"</span>"+" is not a recognized command. Please try again or use <span id=\"highlightSpan\">man</span> for help ;)<br>")
			
			
		}
		$(location).attr('href', '#inp')
	}else if(e.which==38){
		histIndex--;
		if(histIndex<0){
			histIndex=hist.length;
			$("#inp").val("");
		}else{
			$("#inp").val(hist[histIndex]);


		}

		

	}else if(e.which==40){
		histIndex++;
		if(histIndex>hist.length){
			histIndex=0
			$("#inp").val(hist[histIndex]);
		}else{
			$("#inp").val(hist[histIndex]);


		}


	}
	$("#inp").focus();


}
function getFileLinks(prm){
	if (typeof prm =="undefined"){
		var dirs="";
		dirs+="<span id=\"greenSpan\">-</span>"+" <span id=\"highlightSpan\">"+"files"+"<br>&#09;&#09;</span>https://jandourekjaroslav.cz/files<br>"; 	
		dirs+="<span id=\"greenSpan\">-</span>"+" <span id=\"highlightSpan\">"+"scripts"+"<br>&#09;&#09;</span>https://jandourekjaroslav.cz/scripts<br>"; 	
		return dirs;
	}
	prm=prm.replace(/\//g,"");
	if (prm==""){
		var dirs="";
		dirs+="<span id=\"greenSpan\">-</span>"+" <span id=\"highlightSpan\">"+"files"+"<br>&#09;&#09;</span>https://jandourekjaroslav.cz/files<br>"; 	
		dirs+="<span id=\"greenSpan\">-</span>"+" <span id=\"highlightSpan\">"+"scripts"+"<br>&#09;&#09;</span>https://jandourekjaroslav.cz/scripts<br>"; 	
		return dirs;
	}
	results=[]
	var resp=$.ajax({type: "GET", url: prm+"/", async: false});
	filesTxt=resp.responseText;
	if(typeof filesTxt == "undefined" || resp.status !=200){
		var error="command <span id=\"highlightSpan\">ls</span> requires a valid <span id=\"highlightSpan\">directory</span> as a parametr. Run without a parameter for a directory list<br>"
		return error;

	}
	respTxt=filesTxt.match(/<a href=.*<\/a>/g);
	for (var i=2; i<respTxt.length;i++){
		var y= respTxt[i].replace(/<a href=".*">/g,"");
		y=y.replace(/<\/a>/g,"");
		results.push(y)
	}
	var txt="";
	for (var x=0;x<results.length;x++){
		txt+="<span id=\"greenSpan\">-</span>"+" <span id=\"highlightSpan\">"+results[x]+"<br>&#09;&#09;</span>https://jandourekjaroslav.cz/"+prm+"/"+results[x]+"<br>"; 	
	}
	return txt;

}
function cvHandler(prm){
	console.log(prm);
	if (prm==null){
		prm=[];
	}
	switch(prm.length){ 
		case 0:
		case 1:
			$("#out").append("command <span id=\"highlightSpan\">cv</span> requires either <span id=\"highlightSpan\">pdf</span> or <span id=\"highlightSpan\">cli</span> as a parametr<br>");
			break;
		case 2:
			if(prm[1]=="cli"){
				let cv_content=""
				$.get("cv.html", function(data){
					cv_content=data
					$("#out").append(cv_content);
				}); 
			}else if(prm[1]=="pdf"){
				$(location).attr('href', 'https://jandourekjaroslav.cz/cv.pdf');
	
			}else {

				$("#out").append("command <span id=\"highlightSpan\">cv</span> does not support parameter <span id=\"highlightSpan\">"+prm[1]+"</span> <br>");


			}
			break;
		default: 
			 $("#out").append("command <span id=\"highlightSpan\">cv</span> supports only 1 parametr please enter either <span id=\"highlightSpan\">pdf</span> or <span id=\"highlightSpan\">cli</span> as a parametr<br>")
	}

}
function catHandler(prm){
	
	var prmStr=""
	if (prm==null){
		prm=[];
	}
	for (var k=0;k<prm.length;k++){
		prmStr+=prm[k]+" "
	}
	prm=prmStr.match(/[\w\.-]+/g)
	switch(prm.length){ 
		case 0:
		case 1:
			$("#out").append("command <span id=\"highlightSpan\">cat</span> requires a <span id=\"highlightSpan\"> directory&filename (e.g. dir/file) </span> as a parametr <br>");
			break;
		case 2:
			
			$("#out").append("command <span id=\"highlightSpan\">cat</span> requires a <span id=\"highlightSpan\"> directory&filename (e.g. dir/file) </span> as a parametr <br>");
			break;

	
		case 3:
			results=[]
			filesTxt=$.ajax({type: "GET", url: prm[1]+"/", async: false}).responseText;
			respTxt=filesTxt.match(/<a href=.*<\/a>/g);
			for (var i=2; i<respTxt.length;i++){
				var y= respTxt[i].replace(/<a href=".*">/g,"");
				y=y.replace(/<\/a>/g,"");
				results.push(y)
			}
			var found=false;
			for (var x=0;x<results.length;x++){
				if(prm[2]==results[x]){
					found=true;
				}

			}
			if (!found) {
				$("#out").append("File <span id=\"highlightSpan\">"+prm[2]+"</span> does not exist in <span id=\"highlightSpan\">"+prm[1]+"</span><br>");
			}else{
				var filetxt=$.ajax({type: "GET", url: prm[1]+"/"+prm[2], async: false}).responseText	;
				filetxt=filetxt.replace(/\n/g,"<br>");
				$("#out").append(filetxt+"<br>");
			}
			break;
		default: 
			 $("#out").append("command <span id=\"highlightSpan\">cat</span> supports only 1 parametr please enter a filename<br>")
	}




}
