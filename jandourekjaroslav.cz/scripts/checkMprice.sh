curent_data=$( curl -s --location --request GET 'api.coincap.io/v2/assets/' | jq '.data[] | select((.id=="bitcoin") or (.id=="monero") or (.id=="ethereum") or (.id=="litecoin"))' 2>/dev/null)



monero_data=$(echo $curent_data| jq 'select(.id=="monero")')
bitcoin_data=$(echo $curent_data| jq 'select(.id=="bitcoin")')
ether_data=$(echo $curent_data| jq 'select(.id=="ethereum")')
litecoin_data=$(echo $curent_data| jq 'select(.id=="litecoin")')



current_price_mon=""$(echo $monero_data | jq -c .priceUsd|sed 's/\"//g'| sed 's/\..*//g')""
current_price_btc=""$(echo $bitcoin_data | jq -c .priceUsd|sed 's/\"//g'| sed 's/\..*//g')""
current_price_eth=""$(echo $ether_data | jq -c .priceUsd|sed 's/\"//g'| sed 's/\..*//g')""
current_price_ltc=""$(echo $litecoin_data | jq -c .priceUsd|sed 's/\"//g'| sed 's/\..*//g')""


output=" XMR:$current_price_mon \n BTC:$current_price_btc \n ETH:$current_price_eth \n LTC:$current_price_ltc \n"
[[ $bitcoin_data == ""  ]] && output=": %{F#d60606} err_loading_data"
notify-send -u normal " crypto_prices" "$output"
