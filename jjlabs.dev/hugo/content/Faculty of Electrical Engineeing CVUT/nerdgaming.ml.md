---
title: nerdgaming.ml
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: php
http_url: https://gitlab.com/jandourekjaroslav/nerdgaming.ml.git
web_url: https://gitlab.com/jandourekjaroslav/nerdgaming.ml
ssh_url: git@gitlab.com:jandourekjaroslav/nerdgaming.ml.git
description: A simple "forum" like website project made for a webdev class at university
---
# Nerdgaming.ml
A website project for ZWA on CVUT FEL 

