---
title: FEL CVUT - C++
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: cpp
http_url: https://gitlab.com/jandourekjaroslav/fel_cvut_cpp.git
web_url: https://gitlab.com/jandourekjaroslav/fel_cvut_cpp
ssh_url: git@gitlab.com:jandourekjaroslav/fel_cvut_cpp.git
description: Collection of exercises for a C++ class at CVUT
---
