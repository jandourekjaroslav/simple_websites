---
title: CVUT FEL - Object Modeling
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: java
http_url: https://gitlab.com/jandourekjaroslav/fel_cvut_omo.git
web_url: https://gitlab.com/jandourekjaroslav/fel_cvut_omo
ssh_url: git@gitlab.com:jandourekjaroslav/fel_cvut_omo.git
description: Collection of exercises for an "Object Modeling" university class 
---
