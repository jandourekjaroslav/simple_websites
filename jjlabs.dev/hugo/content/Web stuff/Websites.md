---
title: Websites
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: html
http_url: https://gitlab.com/jandourekjaroslav/simple_websites.git
web_url: https://gitlab.com/jandourekjaroslav/simple_websites
ssh_url: git@gitlab.com:jandourekjaroslav/simple_websites.git
description: An aggregate project for the sites i host and maintain
---
