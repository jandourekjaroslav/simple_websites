---
title: jjlabs.dev - Hugo Theme
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: html
http_url: https://gitlab.com/jandourekjaroslav/jjlabs.dev_theme.git
web_url: https://gitlab.com/jandourekjaroslav/jjlabs.dev_theme
ssh_url: git@gitlab.com:jandourekjaroslav/jjlabs.dev_theme.git
description: A simple shopfront/index style theme for Hugo. 

---
# jjlabs.dev hugo theme

This theme can be seen in action on: [jjlabs.dev](https://jjlabs.dev)

It is intended primarily as a "shopfront" style index site for gitlab projects though it can be used in a simmilar manner for anything provided the "header" parameters provided match.

Can be used together with [hugo-importer](https://gitlab.com/jandourekjaroslav/hugo-importer) to export/import a set of Gitlab projects automatically.
