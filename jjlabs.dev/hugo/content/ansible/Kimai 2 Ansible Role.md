---
title: Kimai 2 Ansible Role
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: ansible
http_url: https://gitlab.com/jandourekjaroslav/kimai_ansible.git
web_url: https://gitlab.com/jandourekjaroslav/kimai_ansible
ssh_url: git@gitlab.com:jandourekjaroslav/kimai_ansible.git
description: A simple ansible role to deploy Kimai2 time tracking into docker
---
# kimai_ansible

A simple role to deploy kimai time tracking to docker running on debian
