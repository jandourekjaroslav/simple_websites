---
title: Gitlab Hugo Importer
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: go
http_url: https://gitlab.com/jandourekjaroslav/hugo-importer.git
web_url: https://gitlab.com/jandourekjaroslav/hugo-importer
ssh_url: git@gitlab.com:jandourekjaroslav/hugo-importer.git
description: A simple go program used to export project information into a Markdown/Hugo readable format. 
See it work at: https://jjlabs.dev
---
