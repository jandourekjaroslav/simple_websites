---
title: FEL CVUT - Storage System
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: java
http_url: https://gitlab.com/jandourekjaroslav/fel_cvut_storage_system.git
web_url: https://gitlab.com/jandourekjaroslav/fel_cvut_storage_system
ssh_url: git@gitlab.com:jandourekjaroslav/fel_cvut_storage_system.git
description: A storage inventory and PoS app project made for a university java class
---
# StorageSystem
A project for my Java and Database classes 
