---
title: FEL CVUT - Smart House
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: java
http_url: https://gitlab.com/jandourekjaroslav/fel_cvut_smarthouse.git
web_url: https://gitlab.com/jandourekjaroslav/fel_cvut_smarthouse
ssh_url: git@gitlab.com:jandourekjaroslav/fel_cvut_smarthouse.git
description: An unfinished Smart House simulation uni project
---
