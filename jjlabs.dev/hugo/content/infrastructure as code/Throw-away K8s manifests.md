---
title: Throw-away K8s manifests
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: kube
http_url: https://gitlab.com/jandourekjaroslav/throwaway_kube_manifests.git
web_url: https://gitlab.com/jandourekjaroslav/throwaway_kube_manifests
ssh_url: git@gitlab.com:jandourekjaroslav/throwaway_kube_manifests.git
description: A collection of occasionally useful K8s manifest files
---
