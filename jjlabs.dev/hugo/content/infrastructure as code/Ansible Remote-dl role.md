---
title: Ansible Remote-dl role
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: ansible
http_url: https://gitlab.com/jandourekjaroslav/remote-dl.git
web_url: https://gitlab.com/jandourekjaroslav/remote-dl
ssh_url: git@gitlab.com:jandourekjaroslav/remote-dl.git
description: An Ansible role to configure a node as an off-site downloader. Useful for PIs and such
---
# remote-dl

A simple role to set up a "remote downloader"

Performs the following:
- Sets up a openvpn connection (juuust in case)
- Installs tmux and transmission-cli torrent client
- Sets up an ssh tunnel to a gateway bastion host exposing port 22 to it localy allowing to ssh into the remote-dl host even when hidden behind a nat

2 hosts are required, a remote-dl (a raspberry pi or simillar works quite nicely) and a gateway (probably a vps)
Place into 2 separate groups `endpoint` and `gateway`
