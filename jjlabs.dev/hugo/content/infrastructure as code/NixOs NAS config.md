---
title: NixOs NAS config
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: nix
http_url: https://gitlab.com/jandourekjaroslav/nix-nas.git
web_url: https://gitlab.com/jandourekjaroslav/nix-nas
ssh_url: git@gitlab.com:jandourekjaroslav/nix-nas.git
description: Declarative config for a small NAS with automatic reconciliation of state
---
