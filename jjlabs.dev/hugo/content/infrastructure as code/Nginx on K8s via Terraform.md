---
title: Nginx on K8s via Terraform
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: terraform
http_url: https://gitlab.com/jandourekjaroslav/nginx-kube-terraform.git
web_url: https://gitlab.com/jandourekjaroslav/nginx-kube-terraform
ssh_url: git@gitlab.com:jandourekjaroslav/nginx-kube-terraform.git
description: A simple Terraform definition of an nginx service running in k8s
---
