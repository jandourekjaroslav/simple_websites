---
title: Ansible defined VPS
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: ansible
http_url: https://gitlab.com/jandourekjaroslav/vps.git
web_url: https://gitlab.com/jandourekjaroslav/vps
ssh_url: git@gitlab.com:jandourekjaroslav/vps.git
description: Ansible project to provision and configure a vps server
---
