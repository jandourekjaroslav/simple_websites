---
title: Ansible Davmail role
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: ansible
http_url: https://gitlab.com/jandourekjaroslav/davmail_ansible.git
web_url: https://gitlab.com/jandourekjaroslav/davmail_ansible
ssh_url: git@gitlab.com:jandourekjaroslav/davmail_ansible.git
description: An Ansible role useful to deploy a quick, tls secured davmail container in case of on-prem Exchange incompatibility with the modern world
---
# davmail-ansible

A simple role that deploys a davmail container with a specified config and cert to docker running on a debian system (for Exchange to IMAP/SMTP proxy)


## Requirements

- 2 files placed in the `files/` directory at the root(playbook level) of your project
    - davmail.properties - davmail config (ansible vaulting this would be a good idea)
    - davmai.p12 - Certificate bundle (ansible vaulting this would be a good idea)
