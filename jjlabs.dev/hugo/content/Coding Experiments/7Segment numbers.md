---
title: 7Segment numbers
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: python
http_url: https://gitlab.com/jandourekjaroslav/7segment-numbers.git
web_url: https://gitlab.com/jandourekjaroslav/7segment-numbers
ssh_url: git@gitlab.com:jandourekjaroslav/7segment-numbers.git
description: Tiny coding challenge to print 1-9 as "7"(actually 9) segment numbers in as few lines as possible
---
# Numbers

A simple excercise. Mainle aimed at optimization

Print all the numbers recieved as first argument as though printed on a 7 segment display. 
