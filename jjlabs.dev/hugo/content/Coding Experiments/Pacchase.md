---
title: Pacchase
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: code
http_url: https://gitlab.com/jandourekjaroslav/reversepacman.git
web_url: https://gitlab.com/jandourekjaroslav/reversepacman
ssh_url: git@gitlab.com:jandourekjaroslav/reversepacman.git
description: Small "reverse pacman" game made in highschool
---
# ReversePacman
A game i made as part of my  high school coding final
