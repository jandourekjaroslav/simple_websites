---
title: BattleTetrisArena
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: code
http_url: https://gitlab.com/jandourekjaroslav/battletetrisarena.git
web_url: https://gitlab.com/jandourekjaroslav/battletetrisarena
ssh_url: git@gitlab.com:jandourekjaroslav/battletetrisarena.git
description: PvP Tetris game written with a friend in high school
---
# BattleTetrisArena
 A game i made with a friend as a group project in highschool 
