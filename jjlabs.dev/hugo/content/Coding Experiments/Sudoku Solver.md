---
title: Sudoku Solver
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: python
http_url: https://gitlab.com/jandourekjaroslav/sudoku_solver.git
web_url: https://gitlab.com/jandourekjaroslav/sudoku_solver
ssh_url: git@gitlab.com:jandourekjaroslav/sudoku_solver.git
description: Simple Sudoku solver written in python
---
# Simple sudoku solver written in python
(should have so chosen C or any other **proper language** for pass by value instead of fighting this pass by reference gubbins :man: :palm_tree:)


Takes a file path as a parameter, file format is akin to csv

- ```,``` for delimeter
- ```x``` for empty space
- can be presented either as ```\n``` terminated rows or as a single line with rows serialized (That's how positions are stored internally anyway)
- check the 3 example boards
  + 1st is simple solve (no guessing)
  + other two show of recursion solving with guessing
  + minor optimizations are present (guessing from most likely to find)

