---
title: dotfiles
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: config
http_url: https://gitlab.com/jandourekjaroslav/dotfiles.git
web_url: https://gitlab.com/jandourekjaroslav/dotfiles
ssh_url: git@gitlab.com:jandourekjaroslav/dotfiles.git
description: Collection of config files or "dot" files for some linux tools and utilities
---
