---
title: Fancontrol Kludge
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: config
http_url: https://gitlab.com/jandourekjaroslav/fancontrol-fix.git
web_url: https://gitlab.com/jandourekjaroslav/fancontrol-fix
ssh_url: git@gitlab.com:jandourekjaroslav/fancontrol-fix.git
description: A kludge to fix hwmon devices coming up in random order each boot
---
# fancontrol fix

A quick and dirty fix for the hwmon devices initiliazing in a different order each boot.

Designed for Dell xps13, your milage may vary, a simple fancontrol.temp update should do the trick for most though
