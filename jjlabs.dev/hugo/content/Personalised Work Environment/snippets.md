---
title: snippets
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: code
http_url: https://gitlab.com/jandourekjaroslav/snippets.git
web_url: https://gitlab.com/jandourekjaroslav/snippets
ssh_url: git@gitlab.com:jandourekjaroslav/snippets.git
description: Set of snippets/boilerplates to use as a shortcut
---
