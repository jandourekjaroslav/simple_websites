---
title: Personalised Suckless tools
date: 2023-03-30T20:16:44 -04:00
linkable: true
icon: clang
http_url: https://gitlab.com/jandourekjaroslav/sucklesstoolsbuild.git
web_url: https://gitlab.com/jandourekjaroslav/sucklesstoolsbuild
ssh_url: git@gitlab.com:jandourekjaroslav/sucklesstoolsbuild.git
description: A collection of suckless tools with minor personalised patches
---
