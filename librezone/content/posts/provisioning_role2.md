---
title: "Saving hours of work through refactoring pt.2"
date: 2022-09-01T11:35:49+08:00
tags:
  - ansible
  - linux
  - foreman
  - baremetal
  - ilo
---

Here we go, part 2. This one is going to be super simple.\
No VMware to worry about, all we want is for Foreman to create a new host so that we can get an .iso through the API and then bosh it on the servers management interface to boot from.


 <!--more-->
With that goal in mind here's the first simple edit of the install tasks file


*provision_rhel_baremetal_via_foreman.yml:*
```yaml
---
- name: Check all required vars are defined
  assert:
    that:
      - admin_user is defined
      - admin_password is defined
      - content_source is defined
      - hostgroup is defined
      - location is defined
      - lifecycle_environment is defined
      - content_view is defined
      - operatingsystem is defined
      - kickstart_repository is defined
      - ptable is defined
      - root_pass is defined
      - subnet is defined
      - ip is defined
      - mac is defined
      - ILO_pw is defined
      - ILO_login is defined
      - ILO_host is defined

- name: Check if hosts exist
  theforeman.foreman.host_info:
    name: "{{ inventory_hostname }}"
    server_url: "https://{{ content_source }}"
    username: "{{ admin_user }}"
    password: "{{ admin_password  }}"
  register: foreman_host_info
  changed_when: false
  delegate_to: localhost

- name: Install hosts using foreman
  theforeman.foreman.host:
    name: "{{ inventory_hostname }}"
    build: true
    hostgroup: "{{ hostgroup }}"
    organization: companyredacted
    location: "{{ location }}"
    lifecycle_environment: "{{ lifecycle_environment }}"
    content_view: "{{ content_view }}"
    content_source: "{{ content_source }}"
    environment: puppet
    puppet_proxy: "{{ content_source }}"
    puppet_ca_proxy: "{{ content_source }}"
    architecture: x86_64
    operatingsystem: "{{ operatingsystem }}"
    provision_method: build
    kickstart_repository: "{{ kickstart_repository }}"
    ptable: "{{ ptable }}"
    pxe_loader: PXELinux BIOS
    root_pass: "{{ root_pass }}"
    interfaces_attributes:
      - type: "interface"
        domain: int.companyredacted.cz
        subnet: "{{ subnet }}"
        ip: "{{ ip }}"
        mac: "{{ mac }}"
        provision: true
        managed: true
        primary: true
        name: "{{ inventory_hostname }}"
    ip: "{{ ip }}"
    compute_resource: null 
    compute_profile: null
    server_url: "https://{{ content_source }}"
    username: "{{ admin_user }}"
    password: "{{ admin_password  }}"
    state: present
  delegate_to: localhost
  register: fm_host
  when: foreman_host_info.host == None
  notify:
    - wait for the host to accept connections 
    - wait for porvisioning to finish

- name: Get host foreman id 
  set_fact: 
    fm_id: "{{ fm_host['entity']['hosts'][0]['id']}}" 

```
This is all we really need, set `compute_resource` and `compute_profile` to null. remove all the needless vmware only parameters and add a `mac` address parameter to our network interface.

The reason this is required is two fold, if we used foreman as a DHCP server this is how we tell it to give a cerain box a certain IP at boot. 
This is not the case for us as we are booting via an ISO like in Vmware soooo  the second reason is for Foreman to recognize what server is trying to access it and serve the correct kickstart template. 
It is also part of the Interface recognition/ ip assign kernel parameters that are embeded inside our iso. Servers can have many network cards (duh) but unlike virtual enviroments like vmware we can't easily control what VLAN is assigned to which so direct matching of physical interface to network is needed. 


This is all we really need to do to prepare an .iso for provisioning of bare metal servers. It is not done often so you could be forgiven for just downloading it manually and popping it on the server through iDrac or ILO. I however am lazy so if someone did that work for me i could drink more coffee. Luckily someone basically did.

We use mainly HP servers for any new deployment and as there is a direct module for booting form an .iso file already present in  `community.general` why not use it ? 


Let's add these modules to the end of that task file from above :

```yaml

- name: Boot server from iso image
  community.general.hpilo_boot:
    host: "{{ ILO_host }}"
    login: "{{ ILO_login }}"
    password: "{{ ILO_pw }}"
    media: cdrom
    image: "https://{{ admin_user }}:{{ admin_password }}@{{ content_source }}/bootdisk/api/hosts/{{fm_id}}"
  delegate_to: localhost
  no_log: true
  when: foreman_host_info.host == None

- name: Power off a server
  community.general.hpilo_boot:
    host: "{{ ILO_host }}"
    login: "{{ ILO_login }}"
    password: "{{ ILO_pw }}"
    state: poweroff
  delegate_to: localhost
  no_log: true
  when: foreman_host_info.host == None

``` 

And there we are, after the host is created in Foreman a id is gotten. Then we use that id to point to an .iso generating endpoint in Foreman api. Add a little salt and pepper with auth and just like that we are cooking on gas with the server ready to pop our .iso on next boot. 

Then we just send the poweroff state and with a boot from cdrom queued the server restarts into our prepared installation enviroment and continues on just like the VM would. 


That's it for this part , told you it was quick. 

Next time we'll be looking at a direct opposite of a baremetal linux server. A Windows vm. No foreman involved in that one. Just vmware modules await us. 

See you then, toooodalooo **@jj**  
