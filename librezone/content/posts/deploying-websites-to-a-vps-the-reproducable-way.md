---
title: "Deploying websites to a vps the reproducible way"
date: 2022-08-28T13:11:29+08:00
tags:
  - ansible
  - linux
  - vps
  - hetzner
  - nginx
  - hugo
---

Recently i found myself polishing up a cuple of sites i have been caring for over the past decate or so. \
In that time a lot has changed and i too have learned a lot so it was just the right time to spruce up not only the content but also the deployment. 


Firstly. Being all oldschool and all, i deployed by hand when i first wrote them. That soon changed with a quick and dirty cronjob pulling a git repo every minute or so to make sure everything was up to date. \
That was fine for a couple of years, running in my home *"colocation facility"* first and when i dismatelled that on a VPS.

Not long ago i found myself changing vps providers however and having worked on a lot of automation since the last deployment i found the task of reproducing everything manually so labourious i could cry.

Anyway, that brings us to today and while i am **very** happy with my current hosting provider [1.]({{< ref "deploying-websites-to-a-vps-the-reproducable-way.md#refs" >}}) the specter of a critical failure making me restore from backup still looms. 

Now, i backup pretty rigorously and do a test restore form time to time too. Hetzner makes it extremly easy and a cheeky rsync local backup is covering my jacksie but there is no replacement for some good robust [IaC (2.)]({{< ref "deploying-websites-to-a-vps-the-reproducable-way.md#refs" >}}).
So with that in mind let's change the deployment strategy of my sites (including this one which at the time of writing I'm migrating to [hugo](https://gohugo.io)).

I'll be using Ansible, it's what i'm most fammiliar with, it works well enough and let's me interact with all the nuts and bolts of my services (DNS,Hetzner,nginx and so on). 

With the intro out of the way, let's get to it.

## Software

- ansible
- GitLab CI
- nginx
- certbot

## Goals

By the end our playbooks will ensure the following:

- [ ] A vps server is deployed with a specified OS and in specified config
- [ ] Required packages are installed
- [ ] Deploy the websites into `/var/www/`
- [ ] Setup Virtual Hosts in nginx
- [ ] Generate certificates for each site using Let's Encrypt and setup auto-renewal
- [ ] ... profit ?


## Getting started


### Ansible

First and foremost i already have a git repo setup so we need to make some alterations to make it "Ansible-ready".

I won't be explaining in detail everything i do. There is a certain assumption the reader knows some bash and ansible basics. If not docs and man pages are widely available so feel free to give them a read before continuing.


This is the current repo structure:
```bash
# ~/repositories/vps-sites
 > ls
   other_site1 other_site2 librezone
```

There are many ways to go about this. I'll pick the simplest one and use the [copy]() module.

That means, move all the website folders to a new directory called files and write a playbook that copies them over in a loop
```bash
# ~/repositories/vps-sites
mkdir files
mv * files/
```
Some other directories will be useful too so let's create those

```bash
# ~/repositories/vps-sites
mkdir roles group_vars 
```

We'll be needing some roles and collections for everything to work right. \
I like my repos with batteries included so i tend to download collections and roles and include them directly.
If you like on the fly dependency solving there are ways to do that too listed in the docs. 

Let's create a simple inventory file.

```yaml
---
all:
  hosts:
  	librezone.org:
```

We could use an ini file too but i like to keep yaml where i can so even if it is longer that's what i use. 


Next. Time to get some collections

```bash
ansible-galaxy collection download https://galaxy.ansible.com/hetzner/hcloud
```

After that i'll use a quick deployment role i wrote a while back for my "home_datacenter" repo. 

```bash
git clone https://gitlab.com/jandourekjaroslav/server_provision roles/server_provision
```

Time to start writing ourselves some playbooks.

*deploy.yml:*
```yaml
---
- hosts: all
  gather_facts: false
  become: false
  roles:
    - server_provision
```

Yup that's it, all one needs to do all the deployment work for hetzner. 

I am being a tad dishonest here .... we also need a couple of variables .

If you are unsure about a certain value like location or server_type run the same playbook as above with `-e run=info` and all shall be revealed

*group_vars/all/vars.yml:*
``` yaml
server_type: CX21
image: debian-11
hetzner_location: hel1
hetzner_cloud_ssh_keys: my_super_ssh_key
deploy_with_internal_network: false
provision_dest: hetzner
```
There is also some sensitive info we might want to keep out of the sight of prying eyes.

*group_vars/all/secret.yml:*
``` yaml
root_pw: EXAMPLE_PW
hetzner_api_key: 1234APIKEY
herzner_cloud_new_ssh_key:
  namem: my_super_ssh_key
  public_key: A21J77889Aadf222 example@localhost

```
we also need to encrypt it, luckily ansible has just the tool for the job. 

```bash
ansible-vault ecrypt --ask-vault-password group_vars/all/secret.yml
```
Remember that password, we will need it later for our CI environment. 


### Refs
1. [hetzner ref link](https://hetzner.cloud/?ref=tbMOzY0MLRDL)
1. [Infrastructure as Code](https://en.wikipedia.org/wiki/Infrastructure_as_code)
