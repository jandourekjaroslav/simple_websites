---
title: "Saving hours of work through refactoring"
date: 2022-08-31T11:35:49+08:00
tags:
  - ansible
  - linux
  - foreman
  - vmware
  - vsphere
  - ilo
---

When i started working for the company i now provide services to externaly there was little to no automation to be seen on any infrastructure projects. The idea of having to do things by hand over and over rubs me up the wrong way though so i decided change was needed. First on the list was server provisioning.

Luckily some tools were already present in the infrastructure that would make this at least somewhat painless as all that was required was to configure them correctly.

Being a mainly RHEL shop the content management was done through Foreman. The virtualization platform was plain old vsphere/vcenter by VMWare. 

A handy foreman plugin was all i needed to connect the two for automated provisining through kickstart.
Foreman-bootdisk creates a simple .iso file that is set up to query the Foreman server for the kickstart template as well as configuring networking through VMware API. 

Of to the races we went, no longer needing to provision by hand from a boot DVD iso. 

Good enough to start with not long after I created a quick ansible playbook to call the foreman provisioning service. A little later I incorporated the call of this playbook through AWX via gitlab-ci. (Yes it is a fairly conveluted stack at this point) 

At this point we can comfortably call this setup IaC. Whatever changes are made in the inventory file the provisioning service is called and the deployment altered accordingly. Huzzah. 

Here's the problem though, this daisychain of tooling was created one link at a time out of momentary convenience. It does have some nice perks like being able to provision barematal systems through the same process but that alas is where the convenience ends. 

It's slow, 20 minutes or so to provision a simple set of minimal RH8 vms. Windows being no better taking absolute ages. (Quick side note, windows is handeled differently through a vmware template gold image which presents increased complexity to the set up as it stands.)

Finally the bigest problem i have at the minute is portability. This being an ever changing playbook to accomodate project by project whatever need arises the older projects don't recieve improvements directly without doubling up the work in many places over and over again. 



### Role to the rescue

A provisioning role is the obvious way of going about this. Simple, central repo which anyone can submodule into git projects to deploy any server they need. All the changes carried over to everywhere at time of deployment.

Some refactoring is needed to make this work though so let's get to it. 


First let's create a role template folders sturcture

```bash
ansible-galaxy init companyredacted_provision
```

Now let's take a look at what ansible-galaxy made for us
```
tree companyredacted_provision
companyredacted_provision
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml

8 directories, 8 files

```

This is a base role structure. One can ommit whatever is not needed and we'll be getting to that in a minute too but first let's add some funcitionality. 

The main bulk of all the work is done by the `tasks/main.yml` file. Wanting to provision different types of systems based on a variable value we'll split up the tasks to separate files which we import based on a condition.

*tasks/main.yml*
```yaml
---
- name: Setup env for provision
  import_tasks: setup_provisioning_env.yml

- name: Provision RHEL vm via Foreman
  import_tasks: provision_rhel_via_foreman.yml
  when: >-
    OS is defined and
    OS == "RHEL" and
    system_type is defined and
    system_type == "VM" and
    provision_method is defined and
    provision_method == "foreman"

- name: Provision RHEL baremetal via Foreman
  import_tasks: provision_rhel_baremetal_via_foreman.yml
  when: >-
    OS is defined and
    OS == "RHEL" and
    system_type is defined and
    system_type == "BAREMETAL"

- name: Provision RHEL vm via cloud-init
  import_tasks: provision_rhel_via_cloudinit.yml
  when: >-
    OS is defined and
    OS == "RHEL" and
    system_type is defined and
    system_type == "VM" and
    provision_method is defined and
    provision_method == "cloudinit"

- name: Provision Windows from golden image
  import_tasks: provision_windows.yml
  when: >-
    OS is defined and
    OS == "WINDOWS" and
    system_type is defined and
    system_type == "VM"
```


As it stands we have 4 different provisioning types. 
1. RHEL VM provisioned through kickstart and foreman (the old way)
1. RHEL Bare metal server provisioned through kickstart and foreman (the old way + some nice to haves)
1. RHEL VM provisioned through cloud-init (the new way)
1. Windows VM provisioned from a Golden Image (Windows unfixable crap)


Time to create those provisioning files

First let's get what we have already. And change it to fit with our new role. 

*the old install file: *
{{< details >}}
```yaml
---
- hosts: all
  become: true
  gather_facts: false
  vars:
    version: 1
    companyredacted_content_source: vmsvlforeman.int.companyredacted.cz
  vars_files:
    - vars/secrets.yml

  tasks:
    - name: Check if hosts exist
      theforeman.foreman.host_info:
        name: "{{ inventory_hostname }}"
        server_url: "https://{{ companyredacted_content_source }}"
        username: "{{ admin_user }}"
        password: "{{ admin_password  }}"
      register: foreman_host_info
      changed_when: false
      delegate_to: vmsvlforeman.int.companyredacted.cz

    - name: Install hosts using foreman
      theforeman.foreman.host:
        name: "{{ inventory_hostname }}"
        build: true
        hostgroup: "{{ hostgroup }}"
        organization: companyredacted
        location: location
        lifecycle_environment: "{{ lifecycle_environment }}"
        content_view: RH8Server
        content_source: "{{ companyredacted_content_source }}"
        environment: puppet
        puppet_proxy: "{{ companyredacted_content_source }}"
        puppet_ca_proxy: "{{ companyredacted_content_source }}"
        architecture: x86_64
        operatingsystem: RedHat 8.4
        provision_method: bootdisk
        kickstart_repository: >-
          Red Hat Enterprise Linux 8 for
          x86_64 - BaseOS Kickstart 8.4
        ptable: companyredacted_partition-template_default_general-baseline
        pxe_loader: PXELinux BIOS
        root_pass: "{{ root_pass }}"
        interfaces_attributes:
          - type: "interface"
            domain: int.companyredacted.cz
            subnet: "{{ subnet }}"
            ip: "{{ ip }}"
            provision: true
            managed: true
            primary: true
            name: "{{ inventory_hostname }}"
            compute_attributes:
              network: "{{ subnet }}"
        ip: "{{ ip }}"
        compute_resource: vcenter.int.companyredacted.cz
        compute_profile: companyredacted_Default
        compute_attributes:
          start: "1"
        server_url: "https://{{ companyredacted_content_source }}"
        username: "{{ admin_user }}"
        password: "{{ admin_password  }}"
        state: present
      delegate_to: vmsvlforeman.int.companyredacted.cz
      when: foreman_host_info.host == None
      notify:
        - wait for porvisioning to finish

    - name: Ensure tags are assigned
      community.vmware.vmware_tag_manager:
        tag_names:
          - AvaBackupSTRA
        object_name: "{{ inventory_hostname }}"
        object_type: VirtualMachine
        state: present
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      when: foreman_host_info.host == None

    - name: Ensure Custom atributes are set
      community.vmware.vmware_guest_custom_attributes:
        datacenter: companyredacted_DataCenters
        folder: "/companyredacted_DataCenters/"
        name: "{{ inventory_hostname }}"
        attributes:
          - name: VM:Funkce
            value: "{{ funkce }}"
          - name: VM:Garant companyredacted
            value: "{{ garant_companyredacted }}"
          - name: VM:Garant Externi
            value: "{{ garant_ext }}"
          - name: VM:Install Username
            value: "{{ install_username }}"
        state: present
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      when: foreman_host_info.host == None

    - name: Ensure host parent folder exists in VMWARE
      community.vmware.vcenter_folder:
        datacenter: companyredacted_DataCenters
        folder_name: "{{ parent_folder }}"
        folder_type: vm
        state: present
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      register: folder

    - name: Ensure host folder exists in VMWARE
      community.vmware.vcenter_folder:
        datacenter: companyredacted_DataCenters
        folder_name: "{{ folder_name }}"
        parent_folder: "{{ parent_folder }}"
        folder_type: vm
        state: present
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      register: folder

    - name: Ensure that vms are in correct folder
      community.vmware.vmware_guest_move:
        name: "{{ inventory_hostname }}"
        datacenter: companyredacted_DataCenters
        dest_folder: "/companyredacted_DataCenters/vm/{{ parent_folder }}/{{ folder_name }}"
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      when: foreman_host_info.host == None

    - name: flush_handlers
      ansible.builtin.meta: flush_handlers

    - name: Esure that servers are in correct dc
      community.vmware.vmware_vmotion:
        vm_name: "{{ inventory_hostname }}"
        destination_host: crsvmware01.int.companyredacted.cz
        validate_certs: false
        hostname: "{{ vcenter_hostname }}"
        username: "{{ vcenter_username }}"
        password: "{{ vcenter_password }}"
      delegate_to: vmsvlforeman.int.companyredacted.cz
      when: '"location2" in group_names'

    - name: Ensure the release is set correctly
      ansible.builtin.command: subscription-manager release --set=8
      register: result
      retries: 100
      delay: 10
      until: result.rc == 0
      changed_when: false

  handlers:
    - name: wait for porvisioning to finish
      ansible.builtin.wait_for_connection:
        delay: 1500
      become: false
      delegate_to: localhost
```

{{< /details >}}

As you can see this is awful by nearly any standard, quick, dirty and far too fixed in some areas to be reused. In deep need of a fix. 


Sadly there is no getting away from certian complexity. However...
We can still split up the file to make it a bit easier on the eyes and brain. 

As we will reuse a lot of the vmware modules for all types of deployment bar baremetal we can separate them in to their own task file and import like we do in `main.yml`
Also some input checking is a good idea with that here's the new file

*tasks/provision_rhel_via_foreman.yml:*
{{< details >}}
```yaml
---
- name: Check all required vars are defined
  assert:
    that:
      - admin_user is defined
      - admin_password is defined
      - content_source is defined
      - hostgroup is defined
      - location is defined
      - lifecycle_environment is defined
      - content_view is defined
      - operatingsystem is defined
      - kickstart_repository is defined
      - ptable is defined
      - root_pass is defined
      - subnet is defined
      - ip is defined
      - vcenter_hostname is defined
      - vcenter_username is defined
      - vcenter_password is defined
      - funkce is defined
      - garant_ext is defined
      - garant_companyredacted is defined
      - install_username is defined
      - parent_folder is defined
      - folder_name is defined
      - memory is defined
      - cpus is defined

- name: set params based on vars
  set_fact:
    cluster: "{{ 'cluster1' if 'location1' in location else 'cluster2' }}"


- name: Check if hosts exist
  theforeman.foreman.host_info:
    name: "{{ inventory_hostname }}"
    server_url: "https://{{ content_source }}"
    username: "{{ admin_user }}"
    password: "{{ admin_password  }}"
  register: foreman_host_info
  changed_when: false
  delegate_to: localhost

- name: Install hosts using foreman
  theforeman.foreman.host:
    name: "{{ inventory_hostname }}"
    build: true
    hostgroup: "{{ hostgroup }}"
    organization: companyredacted
    location: "{{ location }}"
    lifecycle_environment: "{{ lifecycle_environment }}"
    content_view: "{{ content_view }}"
    content_source: "{{ content_source }}"
    environment: puppet
    puppet_proxy: "{{ content_source }}"
    puppet_ca_proxy: "{{ content_source }}"
    architecture: x86_64
    operatingsystem: "{{ operatingsystem }}"
    provision_method: bootdisk
    kickstart_repository: "{{ kickstart_repository }}"
    ptable: "{{ ptable }}"
    pxe_loader: PXELinux BIOS
    root_pass: "{{ root_pass }}"
    interfaces_attributes:
      - type: "interface"
        domain: int.companyredacted.cz
        subnet: "{{ subnet }}"
        ip: "{{ ip }}"
        provision: true
        managed: true
        primary: true
        name: "{{ inventory_hostname }}"
        compute_attributes:
          network: "{{ subnet }}"
    ip: "{{ ip }}"
    compute_resource: vcenter.int.companyredacted.cz
    compute_profile: companyredacted_Default
    compute_attributes:
      start: "1"
      cluster: "{{ cluster }}"
    server_url: "https://{{ content_source }}"
    username: "{{ admin_user }}"
    password: "{{ admin_password  }}"
    state: present
  delegate_to: localhost
  when: foreman_host_info.host == None
  notify:
    - wait for porvisioning to finish

- name: Import vmware tasks
  import_tasks: vmware_setup.yml

- name: flush_handlers
  ansible.builtin.meta: flush_handlers

```
{{< /details >}}

I also went ahead and added a small task to setup our new vm with custom compute attributes. Given that foreman deploys from a template we may wish to diverge from ocasionally.

```yaml
- name: Ensure compute attributes are set corectly
  community.vmware.vmware_guest:
    hostname: "{{ vcenter_hostname }}"
    username: "{{ vcenter_username }}"
    password: "{{ vcenter_password }}"
    name: "{{ inventory_hostname }}"
    state: present
    hardware:
      memory_mb: "{{ memory }}"
      num_cpus: "{{ cpus }}"
  delegate_to: localhost
```

Now that all looks good enough for testing. 

I created a simple provisioning playbook which uses the role. Let's take a look at it along with the inventory file. 

{{< details >}}
*install_lin.yml*
```yaml
---
- hosts: all
  become: false
  gather_facts: false
  vars_files:
    - vars/secrets.yml
  roles:
    - companyredacted_provision
```
Isn't that just oh so simple ? :) 

*inventory*
```yaml
---
all:
  hosts:
    deploytest.int.cra.cz:
      ip: 192.168.142.117
      subnet: 3802 Cra_DMZ_Testing
      funkce: "deploytest"
      garant_cra: "garant"
      garant_ext: 'garant'
      install_username: garant
      hostgroup: RH_DEV_FOREMAN
      lifecycle_environment: Dev
      folder_name: DEV
      parent_folder: Vault
      master: true
      OS: RHEL
      system_type: VM
      provision_method: foreman
      memory: 8192
      cpus: 2
```

{{< /details >}}

There are also some baseline default in case an admin just needs the "default product"

We define those in `defaults/main.yml` which looks like this

```yaml
---
content_source: vmsvlforeman.int.companyredacted.cz
hostgroup: RH_DEV_FOREMAN 
location: location
lifecycle_environment: Dev
content_view: rh8
operatingsystem: RedHat 8.4
kickstart_repository: Red Hat Enterprise Linux 8 for x86_64 - BaseOS Kickstart 8.4
ptable: companyredacted_partition-template_default_general-baseline
subnet: companyredacted_DMZ_Testing
```

Let's run this baby. 

```bash
ansible-playbook -i inventory install_lin.yml
#.
#.
#.

PLAY RECAP ************************************************************************************
deploytest.int.companyredacted.cz      : ok=1    changed=12    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

**SUCCESS**


So with that finished we have basic functionality done to match the current playbook. We can publish the role and have admins use it to provision the same VMs from our centralised role. 

Part 2 comming up where we'll just bosh out a quick baremetal version and Part 3 will be *yuck* Windows...

Stay tunned though cause Part 4 has the all new cloud init deployment style aaand we might even delve into some Vcloud Director. 
