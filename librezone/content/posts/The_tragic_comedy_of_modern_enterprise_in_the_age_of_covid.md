---
title: "The tragic comedy of modern enterprise in the age of covid"
date: 2021-03-03T17:48:17+08:00
tags: 
  - meetings
  - linux
  - admin
  - enterprise
  - rant 
---

When I first started working in tech for "real" money, the world was simple. I was just out of secondary school and one could make a decent living making the odd website without much complexity to it. Sure even then there were ramblings of "responsive" sites and what not, but most of the projects I took were pure HTML and CSS. Simple, easy, quick. Some wanted to get a bit fancy which could usually get resolved with sub 100 lines of javascript on the client end. Of course forms exited and php was a thing but it was all simple as pie. It was a better time, time of less bloat and far less random corporate surveillance.

![react_shite](/img/react_shite.jpg)



But the state of the web is something we can bitch and whine about till the cows come home and nothing will get solved. The reason I mention my start in tech is this, back then whatever needed to be done could be described to me in 30 minutes, completed over a weekend and subsequently tweaked as needed with feedback coming through email. That was it. No grand sync meetings involving a committee of cunts, no reports of time worked or contracts of SLAs and other nonsense garbage. Someone wanted a website to be X so I made X , or at least what I perceived X to be, sent it back and changed the things I got wrong or didn't work as nicely in reality as they did in the mind that came up with them. At the end of it all the work was done, no time was wasted and the product was correct. Job done, invoice paid, email for later contact already in the virtual Rolodex.

There are many project that run like this to this day (Mostly open source ones not backed by corporations although there sure are exceptions), of course more people means more complexity in management but we all sit on the same git repo, have the same view of issues and their assignments and can just get on with whatever needs doing. We still send off the odd email, probably to a mailing list for the project and if more real time discussion is to be had we hop on IRC or whatever modern equivalent is chosen (looking at you needlessly bloated Matrix-verse hint hint) and sort stuff out as it's happening. Yet again, tasks get done, milestones reached, releases pushed and all is well in the world.

![happy_days](/img/happy_days.png)



However, in the space of corporations that needed to adapt to "work from home" as the pandemic loomed, this very obvious solution rarely got accepted. "How is work gonna get done without management oversight?!" they screamed, as they scrambled to retain the same "all in one building, chatting about nothing in the kitchenette while slagging off Barbara for wearing an ugly sweater that shows too much cleavage" mentality and "work" process. "MEEEEETINGS" was the call throughout the enterprises of the land, if we can't talk in person we shall do it in "MEEEEEETINGS". And so through idiocy Zoom became what it is today because we still needed to find a place where we can talk about Barbara and her tits.

These days I no longer make websites, not for money anyway, I stopped as the age of frameworks and perpetual ever present javascript began, my heart simply wasn't in it. I transitioned to server management, the bastion of the traditional and sagely cautious Linux mages that dwell in high towers of server farms looking down on ordinary peasant users with disdain. I like working with servers, I like that for days at a time I didn't have to talk to a single person at work, just get stuff working when the ticket came or the issue was assigned. Believe it or not even DevOps doesn't bother me, I think it's a nifty solution to the perpetual snowflake server problem, "define it as code and redeploy whenever you need to instead of wasting hours on recovery from tape" that is a great principal to work with as both a Linux server guy and a coder at heart, even though it was brought upon by the age of cloud and it's misty shitassery (Yes I am the "it's somebody else's computer" guy, quelle surprise ).

![olympus](/img/mount-olympus.jpg)

But, here comes covid and for all the hurt it's caused, this is hardly the biggest thing to put on the rap sheet, none the less it is to blame here as well, although as nothing more then a trigger. The age of "75% of any work day are fucking meetings". How far we have fallen from our server farm Olympus, how badly it hurts to get something started only for the notification to pop up that yet another manager demands my presence during an hour of pointless drivel.

I changed jobs mid pandemic. I was called crazy for giving up the stability of a good position but I just couldn't hack it any more with all the work I was constantly prevented from doing. I thought "surely this stupidity is only here, at this company full of 'water cooler' people. There is bound to be a mountain of solitude out there somewhere waiting for me, with all but IRC and email for means of communication. "

The reality of my naive idealism struck hard wherever I interviewed. The same thing came up over and over again, meeting after meeting, there was no mountain of solace I could go back to, everywhere the manager would gleefully praise whatever meeting solution they had, "it's almost like working in the office, we all get to chat all the time the only bit missing is the catering(**HA HA ha h h a**)". My soul was distraught, broken and so with gritted teeth I accepted reality, found a place which was willing to pay the best for the suffering and signed up.

![facepalm](http://3.bp.blogspot.com/-UDpZdNB0bbo/U15j4lKrTTI/AAAAAAAAEHA/MG1e1K6HkyQ/s1600/PicardDoubleFacepalm-1.jpg)

So here I am, unable to work, sitting in a LYNC (no joke) meeting with a contractor, half a dozen project management staff, and 3 other bored admins watching how one clicks on folders in Sharepoint, all the management loving it and asking questions like it's a show and tell day at a pre-school. Hundreds of thousands of company money in man hours disappearing down the shitter. The cocktractor rubs his hands at the ease of this extremely long and juicy billable time. The company itself is dying, running out of money after a year of covid time wasting. No more Laptops for next fiscal year they say. Oh well, I will just sit and watch it burn because that is what I was hired for. Work isn't on the list of my duties here and neither is it on anyone else's any more. Enjoy the coffee and surf the web (whilst getting angry at the state of the fucking thing), there is no point in listening anyway. To all my brothers and sister of that great mythical mound, may we return one day together to again enjoy our work alone.

**(Written while in a genuine meeting to retain sanity.) - Toooodalooo @jj**
