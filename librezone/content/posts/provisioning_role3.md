---
title: "Saving hours of work through refactoring pt.3"
date: 2022-09-01T14:35:49+08:00
tags:
  - ansible
  - windows
  - vmware 
  - vm
---

Part 3 is upon us and though i am really not looking forward to wating 3 hours for the test build of windows to finish it is an important step to the true point of this series. 

Firsly, a recap. 

In parts 1 and 2 we delt with some refactoring of existing playbooks to deploy linux boxes on vmware as vms and baremetal HP servers. This was done mostly for convenience sake of the admins needing to provision servers so that they can use one utility to do all the required provisioning rather than copying and changing the same playbooks over and over again. In effect we created a library of sorts that provisions servers based on parameters provided and let's one only deal with the deployment customization at OS level and above.


This time we tackle Windows, which as you'll see is done a bit differently. 

Firsly, no foreman. You can inject windows .isos into foreman but it is a huge pain and not efficient to work with after at all. 

Instead we are going to use a vm template inside vcenter itself. \
This is an important step, for one it is not that different from what awaits us in part 4. \
Namely, cloud-int provisioning of linux vm boxes .

There we will be using the aformentioned cloud-init to do our customization. \
Here however we will let a vmware template and customization spec do the work. \
You will see that the differences are rather slight in the end if one disregards the setup needed before hand. 


Let's get to it.

We define a task file like before. Keeping the name we decided on on part 1.

*provision_windows.yml*

```yaml
---
- name: Check all required vars are defined
  assert:
    that:
      - subnet is defined
      - ip is defined
      - vcenter_hostname is defined
      - vcenter_username is defined
      - vcenter_password is defined
      - funkce is defined
      - garant_ext is defined
      - garant_companyredacted is defined
      - install_username is defined
      - parent_folder is defined
      - folder_name is defined
      - memory is defined
      - cpus is defined
      - windows_template is defined
      - dns_suffix is defined
      - dns_servers is defined
      - netmask is defined
      - gateway is defined
      - domain is defined
      - domain_join_user is defined
      - domain_join_pw is defined
      - win_password is defined
      - win_user is defined
      - drives is defined

- name: Install host using vmware template
  community.vmware.vmware_guest:
    datacenter: companyredacted_DataCenters
    folder: /companyredacted_DataCenters/vm
    name: "{{ inventory_hostname }}"
    template: "{{ windows_template }}" 
    disk: "{{ drives }}"
    hardware:
      memory_mb: "{{ memory }}"
      num_cpus: "{{ cpus }}"
      num_cpu_cores_per_socket: 1
      scsi: lsilogicsas
      memory_reservation_lock: true
      mem_limit: "{{ memory }}"
      mem_reservation: 4096
      cpu_limit: 8096
      cpu_reservation: 4096
      max_connections: 5
      hotadd_cpu: true
      hotremove_cpu: true
      hotadd_memory: true
      version: 13
      boot_firmware: "efi"
    networks:
      - name: "{{ subnet }}"
        connected: true
        ip: "{{ ip }}"
        netmask: "{{ netmask }}"
        gateway: "{{ gateway }}"
        dns_servers: "{{ dns_servers }}"
        type: static
        domain: "{{ domain }}"
    wait_for_ip_address: true
    wait_for_customization: true
    customization:
      existing_vm: true
      dns_servers:
      dns_servers: "{{ dns_servers }}"
      dns_suffix: "{{ dns_suffix }}"
      domain: "{{ domain }}"
      hostname: "{{ inventory_hostname | regex_replace('\\..*companyredacted.cz$') }}"
      timezone: 095
      joindomain: "{{ domain }}"
      domainadmin: "{{ domain_join_user }}"
      domainadminpassword: "{{ domain_join_pw }}"
      password: "{{ win_password }}"
      fullname: "{{ win_user }}"
    hostname: "{{ vcenter_hostname }}"
    username: "{{ vcenter_username }}"
    password: "{{ vcenter_password }}"
    validate_certs: false
    state: poweredon
  delegate_to: localhost
  register: deploy


- name: Import vmware tasks
  import_tasks: vmware_setup.yml

- name: Run command inside a virtual machine
  community.vmware.vmware_vm_shell:
    datacenter: companyredacted_DataCenters
    folder: "/companyredacted_DataCenters/vm/{{ parent_folder }}/{{ folder_name }}"
    vm_id: "{{ inventory_hostname }}"
    vm_username: "{{ win_user }}"
    vm_password: "{{ win_password }}"
    vm_shell: 'C:/windows/system32/windowspowershell/v1.0/powershell.exe'
    vm_shell_args: "{{ item }}"
    validate_certs: false
    hostname: "{{ vcenter_hostname }}"
    username: "{{ vcenter_username }}"
    password: "{{ vcenter_password }}"
    wait_for_process: true
  delegate_to: localhost
  retries: 100
  delay: 30
  register: shell_command_output
  loop:
    - 'powershell { Invoke-WebRequest -Uri
      http://vmsvlforeman.int.companyredacted.cz/pub/ConfigureRemotingForAnsible.ps1
      -OutFile c:/cfg.ps1 }'
    - 'powershell -ExecutionPolicy ByPass -File c:/cfg.ps1'
    - 'powershell { Remove-Item c:/cfg.ps1 }'

```

Like before we check all our required parameters are defined. Simple enough. 
Some sensible defaults are also a good idea so we pop those in `defaults/main.yml` yet again.
As most are windows specific it doesn't really hurt having them set for linux machines as well, they are just not used. 

Next we make use of the same `community.vmware` collection we use in our `vmware_setup.yml` task file to create a new VM from the requested template. 

We make sure to include `customization` parameters to get our new vm registered to a domain as well as for networking to be setup.

Then like before we do all the `vmware_setup.yml` tasks to get some consistency in our vcenter structure.

Finally a command is run through vmware tools integration. Or rather a set of commands. 
All of these serve the same goal. Configuring winrm so that we can continue to modify the windows vm through Ansible.

And that's it. After waiting for about 30 minutes (i was a bit facisious at the begining there) we have a host setup and ready for configuration. 

```bash
PLAY RECAP *********************************************************************************************************
deploytest.int.cra.cz      : ok=9    changed=7    unreachable=0    failed=0    skipped=17   rescued=0    ignored=0
```
You may notice that a lot of tasks start to get skipped at this point. These are all the tasks intended for linux provisioning and hence they get ignored during execution. 

The nifty thing is this. If our inventory included 3 different hosts each using a different provisioning method.1 baremetal, 1 foreman created linux vm , 1 windows all would be provisioned in the same run. 
Yet another perk of using a role rather than discrete playbooks for each task. Huzzah.

See you later, toooodalooo **@jj**  
