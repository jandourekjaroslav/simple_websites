---
title: "Saving hours of work through refactoring pt.4"
date: 2022-09-06T14:35:49+08:00
tags:
  - ansible
  - linux
  - vmware 
  - cloud-init
  - vm
---


Howdy, as you can probably tell from the timestamp this part took a lot longer than i hoped. 

Firstly let me outline what i'm trying to achieve in this part. 

1. Deploy a RHEL 8 image on a vSphere/vcenter/esxi set of hypervisors.
1. Do it all in ansible
1. Do it using cloud-init
1. Do it as streamlined as possible to limit overhead in provisioning and get the speed up from the foreman alternative.


So point one was quite easy, the `deploy from template` funcionality is very old and well established in the vmware ecosystem. Essencially it is a pure copy of the windows provisioning task in part 3. 

VMWare bundles a set of guest customization tools with vSphere that we used with Windows also. Unlike Windows hover these are simply not powerful enough for my usecase in Linux. This is where cloud-init comes in.

As a far more robust provisioning template it allows for a TON of functionality. It is also the defacto standard for most cloud-providers with good integrations for most if not all. But. VMWare has been seemingly asleep at the wheel as the support for their integration with cloud init didn't appear till **version 21.3 !**.

And let me tell you, all the pain anyone who ever worked with vmware tooling knows is here as much as anywhere else. 

## VMWare oh how i loathe thee

Let's start with the documentation..... Non Existant. Not just for the `vmware` cloud-init data source but also for vcenter and crucially for the community.vmware collection. 

An incredibly vague walkthrough of using a vmware tool to modify an already existing VM from the cli exists in cloud-init's docs but that's about it. THe link to the object in vmware docs is basically useless as far as setting. It just describes the datatype and no API endpoints are linked what would allow one to set the property.

The worst thing is this however. Naming. For VMWare, the same thing is called 4 different things depending on where you look. 

In vcenter, there is something called "Advanced Settings", cloud-init docs (written by vmware employees) mention"extraConfig" properties, and the ansible collection provided by vmware has 3 different fields all of which could fit for setting the cloud-init property without any documentation to them at all. 

The ONLY way to figure out what one needs to do is to read the code(python) for the `community.vmware.vmware_guest` module. Luckily being open source it's all on github. 

If one reads the funciton thoroughly they notice that if a list of key-value dictionaries is provided in the advanced_settings parameter of the module this in fact results in those values being passed as *extraConfig* properties to the VM and appear in the advance settings parameters of the VM.

Let me say that again the **only** way to connect the dots is not using official documentation but **rummiging through module code** to see what each property actually sets and how that affects one's enviroment. 


## RTFM doesnt work when M is not defined mate 

Point 2 was hence the most difficult hurdle. One could settle for using terraform where the heavy lifting is done by their implementation and after taking 3 days to igure out the process with ansible i would honestly recomend going that way instead if you at all can. I alas could not, Terraform is not supported in my work enviromnet and hence i could not use it. 

There are many guides on how to achieve exactly the same thing i am but they fall to one of 3 categories. 
1. Use terraform (sadly i can't)
1. Use a nocloud datasource (Generating an iso you mount at creation of vm for the data to be passed. Clunky ass all hell and beieve me i tried and while it does work it is an awful dependecy hell and not elegant at all)
1. Edit an ovf template and deploy that (Utterly useless if you don't want to upload giggs upon gigs of template data per VM)

## Sweet release of getting the garbage to run

So points 2-4 were looking hella shaky until i read the module code. But after. Boy did i make it fly. 

40 VMs created and ready for apps in 3.2 minutes total time. Exaclty what i was after all along. 

Lemme show you the code and you be the judge if i am just stupid or if this is an utterly unheard of usecase. Also if i am just in hating VMWare with a buring passion for their god awful way of documenting their tooling. 


*provision_rhel_via_cloud_init.yml (just the main task the rest are repeated)*

```yaml
- name: Install host using vmware template and cloud-init
  community.vmware.vmware_guest:
    datacenter: companyredacted_DataCenters
    folder: /companyredacted_DataCenters/vm
    name: "{{ inventory_hostname }}"
    template: "{{ linux_template }}" 
    disk: "{{ drives }}"
    hardware:
      memory_mb: "{{ memory }}"
      num_cpus: "{{ cpus }}"
      num_cpu_cores_per_socket: 1
      scsi: lsilogicsas
      memory_reservation_lock: true
      mem_limit: "{{ memory }}"
      mem_reservation: "{{ memory }}"
      cpu_limit: 8096
      cpu_reservation: 4096
      max_connections: 5
      hotadd_cpu: true
      hotremove_cpu: true
      hotadd_memory: true
      version: 13
      boot_firmware: "efi"
    networks:
      - name: "{{ subnet }}"
        connected: true
        ip: "{{ ip }}"
        netmask: "{{ netmask }}"
        gateway: "{{ gateway }}"
        dns_servers: "{{ dns_servers }}"
        type: static
        domain: "{{ domain }}"
    advanced_settings:
      - key: guestinfo.metadata
        value: "{{ lookup('ansible.builtin.template', 'templates/network_config.cfg.j2') | string | b64encode }}"
      - key: guestinfo.metadata.encoding
        value: "base64"
      - key: guestinfo.userdata 
        value: "{{ lookup('ansible.builtin.template', 'templates/user-data.cfg.j2') | string | b64encode }}"
      - key: guestinfo.userdata.encoding 
        value: "base64"
    hostname: "{{ vcenter_hostname }}"
    username: "{{ vcenter_username }}"
    password: "{{ vcenter_password }}"
    validate_certs: false
    state: poweredon
  delegate_to: localhost
  register: deploy
```

See it ? Literally as simple as adding a single list of KVs. 

```yaml
    advanced_settings:
      - key: guestinfo.metadata
        value: "{{ lookup('ansible.builtin.template', 'templates/network_config.cfg.j2') | string | b64encode }}"
      - key: guestinfo.metadata.encoding
        value: "base64"
      - key: guestinfo.userdata 
        value: "{{ lookup('ansible.builtin.template', 'templates/user-data.cfg.j2') | string | b64encode }}"
      - key: guestinfo.userdata.encoding 
        value: "base64"

```

This takes a couple of templates, encodes them in base64 and pops them in the *advanced_settings/extraconfig/advanced_parameters* fields of our VM. 
The templates follow basic cloud-init syntax i won't go into here. The metadata file also lists network configuration for the VM. The user-data all other configuration for whatever your needs are. 

## The pre steps needed and random hurt

Several things are also needed before this is usable. 
1. A template with cloud-init (of the right version a real problem on older RHEL) installed and it's service enabled.
1. A configuration change in `/etc/cloud/cloud.cfg` to turn off the unneeded guest customization by vmware


You may wonder why network config is still present in the module when the metadata should cover the setup. Simple, it makes it faster. 

If you set only the vmware switch with name and demand it be connected the machine will take 2 full minutes to give up on trying to make NetworkManager come up. This is a yawn fest and i'm too tired to figure out how to stop it. So the easy fix route i went. Put the parameters in there and let VMware set them before cloud init runs and cemments them properly into all the necessary config files.


## Conclusion and moving on

Initially my plan was to include VcloudDirector integration also. It is an edge use case for my company as cloud is a *no no* word around here but used out of necesity once in a while nonetheless. I do have a playbook ready and working but i won't burden you with it. It uses the python library from vmware and their modules which could at best be described as early alpha. In fact they gave me the idea to check the code in the first place for this part because with VCD modules it is an even grater necesity as docs aren't updated at all. 

If you are looking for ways to manage VCD through automation, USE TERRAFORM for the love of all that is good. They deal with VMWare for you and you don't have to. 

If you are a glutten for punishment use their bare API. They have 2. Each documented often wrongly, in 4 different places across their sites. You will need every single one of those docs to even create a single vm. (I know, trust me on a completely different project i deal with VCD garbage exclusively for automation for a cloud provider. Funny isn't it ?)

Finnally. There are BETTER OPTIONS OUT THERE. KVM is a breeze to work with, openstack gives you a good gui and cluster support or use Proxmox. If you have the choice through ANY and EVERY tool VMWare makes onto the scrapheap and stay the fuck away for the benefit of your own sanity. 


*sigh* **@jj**
