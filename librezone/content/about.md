---
title: "About"
date: 2022-08-28T11:57:38+08:00
---

Education gives sobriety to the young, comfort to the old, riches to the poor and is an ornament to the rich.

\- _Diogenes_

Broadly speaking this is a blog.\
Specifically speaking this is me writing nonsense but it helps me learn and it might help others too so why not right?

 <!--more-->
