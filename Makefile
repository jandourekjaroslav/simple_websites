.DEFAULT_GOAL := deploy-all

install:
	grep simple_websites /etc/crontab || echo "5 *	* * *	root	git clone --recurse-submodules https://gitlab.com/jandourekjaroslav/simple_websites /tmp/websites && cd /tmp/websites && make && rm -rf /tmp/websites" >> /etc/crontab
conf-nginx:
	rsync -ruv nginx_conf/* /etc/nginx/conf.d/
regenerate-certs:
	certbot renew
deploy-librezone: /usr/bin/hugo
	cd librezone && hugo
	rsync -ruv librezone/public/* /var/www/librezone_blog/
deploy-jj:
	rsync -ruv jandourekjaroslav.cz/* /var/www/html/
deploy-labs: /usr/bin/hugo-importer /usr/bin/hugo
	hugo-importer 'https://gitlab.com/api/v4/users/jandourekjaroslav/projects' 'jjlabs.dev/hugo/content/'
	cd jjlabs.dev/hugo/ && hugo
	rsync -ruv jjlabs.dev/hugo/public/* /var/www/jjlabs.dev/
deploy-curlables:
	rsync -ruv curlables/* /var/www/html/curl/
deploy-arttam:
	rsync -ruv arttam/* /var/www/arttam/
/usr/bin/hugo:
	apt update
	apt install hugo
/usr/bin/hugo-importer:
	wget https://gitlab.com/jandourekjaroslav/hugo-importer/-/package_files/73613022/download && mv download hugo-importer
	chmod +x hugo-importer
	cp hugo-importer /usr/bin/hugo-importer

deploy-all: install deploy-labs
